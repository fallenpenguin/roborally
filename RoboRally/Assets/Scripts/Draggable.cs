﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	public Transform originalParent = null;
	public Transform placeholderParent = null;
	
	GameObject placeholder = null;

	/// <summary>
	/// Creates placeholder on BeginDrag and makes dragged object passable for raycasts.
	/// </summary>
	/// <param name="eventData">BeginDrag event data.</param>
	public void OnBeginDrag(PointerEventData eventData) {
		placeholder = new GameObject();
		placeholder.transform.SetParent( this.transform.parent );
		LayoutElement le = placeholder.AddComponent<LayoutElement>();
		le.preferredWidth = this.GetComponent<LayoutElement>().preferredWidth;
		le.preferredHeight = this.GetComponent<LayoutElement>().preferredHeight;
		le.flexibleWidth = 0;
		le.flexibleHeight = 0;
		
		placeholder.transform.SetSiblingIndex( this.transform.GetSiblingIndex() );
		
		originalParent = this.transform.parent;
		placeholderParent = originalParent;
		this.transform.SetParent( this.transform.parent.parent );
		
		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}

	/// <summary>
	/// Ensures correct animation and order in drop area for dragged object.
	/// </summary>
	/// <param name="eventData">Drag event data.</param>
	public void OnDrag(PointerEventData eventData) {
		this.transform.position = eventData.position;
		
		if(placeholder.transform.parent != placeholderParent)
			placeholder.transform.SetParent(placeholderParent);
		
		int newSiblingIndex = placeholderParent.childCount;
		
		for(int i=0; i < placeholderParent.childCount; i++) {
			if(this.transform.position.x < placeholderParent.GetChild(i).position.x) {
				
				newSiblingIndex = i;
				
				if(placeholder.transform.GetSiblingIndex() < newSiblingIndex)
					newSiblingIndex--;
				
				break;
			}
		}
		
		placeholder.transform.SetSiblingIndex(newSiblingIndex);
		
	}

	/// <summary>
	/// Replaces card in correct location and destroys placeholder.
	/// </summary>
	/// <param name="eventData">EndDrag event data.</param>
	public void OnEndDrag(PointerEventData eventData) {
		this.transform.SetParent( originalParent );
		this.transform.SetSiblingIndex( placeholder.transform.GetSiblingIndex() );
		GetComponent<CanvasGroup>().blocksRaycasts = true;
		
		Destroy(placeholder);
	}
}
