﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	enum Direction {North, East, South, West};

	public static Player playerInst = null;

	public float moveTime = 0.1f;
	private float inverseMoveTime;
	private bool ongoingMovement=false;

	private Card[] register;
	private Card[] hand;
	private Vector2 position;
	private Vector2 nextPosition;
	private Vector2 respawn;
	private Direction direction;
	private int hp=10;


	void Awake () {
		if (playerInst == null) playerInst = this;
		else if (playerInst != null) Destroy(gameObject);

		inverseMoveTime = 1f / moveTime;

		GetComponent<SpriteRenderer>().enabled=true;
		register=new Card[5];
		hand=new Card[9];
		respawn=new Vector2(0f,0f);
		respawnPlayer();
	}

	/// <summary>
	/// Respawns the player.
	/// </summary>
	public void respawnPlayer() {
		position=respawn;
		nextPosition=respawn;
		hp=10;
		direction=Direction.North;
		GetComponent<Rigidbody2D>().rotation=90;
		StartCoroutine(SmoothMovement(respawn));
	}

	/// <summary>
	/// Sets the respawn.
	/// </summary>
	/// <param name="location">Coordinates of respawn location.</param>
	public void setRespawn(Vector2 location) {
		respawn=location;
	}

	/// <summary>
	/// Sets card in hand at specified index.
	/// </summary>
	/// <param name="index">Index to set.</param>
	/// <param name="card">Card to set.</param>
	public void setHand( int index, Card card) {
		if (index < 9) hand[index]=card;
		else Debug.Log("setHand index too high: "+index);
	}

	/// <summary>
	/// Gets the card at the specified index in hand.
	/// </summary>
	/// <returns>The card at the specified index.</returns>
	/// <param name="index">Index to get card from.</param>
	public Card getHand(int index) {
		if (index < 9) return hand[index];
		else {
			Debug.Log("Returned index 0, getHand index too high: "+index);
			return hand[0];
		}
	}

	/// <summary>
	/// Sets the register at the specified index.
	/// </summary>
	/// <param name="index">Index to set.</param>
	/// <param name="card">Card to set.</param>
	public void setRegister( int index, Card card) {
		if (index < 5) register[index]=card;
		else Debug.Log("setRegister index too high: "+index);
	}

	/// <summary>
	/// Checks, if the player wins.
	/// </summary>
	public bool wins() {
		if (GameManager.instance.getTileType(position).Contains("Checkpoint")) return true;
		return false;
	}

	/// <summary>
	/// Gets the register priority.
	/// </summary>
	/// <returns>The register priority.</returns>
	/// <param name="number">Index of register to check.</param>
	public int getRegisterPriority(int number) {
		return register[number].Priority;
	}

	/// <summary>
	/// Executes the specified player register and board elements.
	/// </summary>
	/// <returns><c>true</c>, if player survived, <c>false</c> if player died.</returns>
	public bool executeRegister(int number) {
		if (register[number].Label.Contains("Move")) {
			if (register[number].Label.Contains("1")) {
				move(direction, canMoveHowFar(direction, 1));
			}

			if (register[number].Label.Contains("2")) {
				move(direction, canMoveHowFar(direction, 2));
			}
			else {
				if (register[number].Label.Contains("3")) {
					move(direction, canMoveHowFar(direction, 3));
				}
			}
		}
		else if (register[number].Label.Contains("Back")) {
			move(ReverseDirection(direction), canMoveHowFar(ReverseDirection(direction), 1));	
		}
		//Turns
		else turn(register[number].Label);

		return doBoardElements();
	}

	/// <summary>
	/// Executes the board elements.
	/// </summary>
	/// <returns><c>true</c>, if player survived, <c>false</c> if player died.</returns>
	private bool doBoardElements() {
		string currentTileType=GameManager.instance.getTileType(position);

		//check hole or borders
		if ((currentTileType.Contains("Hole")) || (currentTileType.Contains("Border"))) {
			respawnPlayer();
			return false;
		}

		//blue conveyors move
		if (currentTileType.Contains("BConv")) {
			if ((currentTileType.Contains("Left")) || (currentTileType.Contains("DL")) || (currentTileType.Contains("UL"))) {
				move(Direction.West, 1);
			}
			else if ((currentTileType.Contains("Right")) || (currentTileType.Contains("DR")) || (currentTileType.Contains("UR"))) {
				move(Direction.East, 1);
			}
			else if ((currentTileType.Contains("Up")) || (currentTileType.Contains("LU")) || (currentTileType.Contains("RU"))) {
				move(Direction.North, 1);
			}
			else if ((currentTileType.Contains("Down")) || (currentTileType.Contains("LD")) || (currentTileType.Contains("RD"))) {
				move(Direction.South, 1);
			}

			currentTileType=GameManager.instance.getTileType(position);
			if ((currentTileType.Contains("DL")) || (currentTileType.Contains("LU")) || (currentTileType.Contains("RD")) || (currentTileType.Contains("UR"))) {
				turn("Left");
			}
			else if ((currentTileType.Contains("DR")) || (currentTileType.Contains("LD")) || (currentTileType.Contains("RU")) || (currentTileType.Contains("UL"))) {
				turn("Right");
			}
		}

		//all conveyors move
		if (currentTileType.Contains("Conv")) {
			if ((currentTileType.Contains("Left")) || (currentTileType.Contains("DL")) || (currentTileType.Contains("UL"))) {
				move(Direction.West, 1);
			}
			else if ((currentTileType.Contains("Right")) || (currentTileType.Contains("DR")) || (currentTileType.Contains("UR"))) {
				move(Direction.East, 1);
			}
			else if ((currentTileType.Contains("Up")) || (currentTileType.Contains("LU")) || (currentTileType.Contains("RU"))) {
				move(Direction.North, 1);
			}
			else if ((currentTileType.Contains("Down")) || (currentTileType.Contains("LD")) || (currentTileType.Contains("RD"))) {
				move(Direction.South, 1);
			}
			
			currentTileType=GameManager.instance.getTileType(position);
			if ((currentTileType.Contains("DL")) || (currentTileType.Contains("LU")) || (currentTileType.Contains("RD")) || (currentTileType.Contains("UR"))) {
				turn("Left");
			}
			else if ((currentTileType.Contains("DR")) || (currentTileType.Contains("LD")) || (currentTileType.Contains("RU")) || (currentTileType.Contains("UL"))) {
				turn("Right");
			}
		}

		//gears turn
		if (currentTileType.Contains("Gear")) {
			if (currentTileType.Contains("Left")) {
				turn("Left");
			}
			else if (currentTileType.Contains("Right")) {
				turn("Right");
			}
		}

		//lasers fire
		if (currentTileType.Contains("Laser")) {
			hp--;
			if (hp==0) {
				respawnPlayer();
				return false;
			}
		}

		//repairs
		if (currentTileType.Contains("Wrench")) {
			if (hp<10) hp++;
		}

		return true;
	}

	/// <summary>
	/// Executes turn to specified direction.
	/// </summary>
	/// <param name="turnDir">Direction to turn to.</param>
	private void turn(string turnDir) {
		if (turnDir.Contains("Left")) {
			GetComponent<Rigidbody2D>().rotation += 90;
			if (direction==Direction.North) direction=Direction.West;
			else if (direction==Direction.East) direction=Direction.North;
			else if (direction==Direction.South) direction=Direction.East;
			else if (direction==Direction.West) direction=Direction.South;
		}
		else if (turnDir.Contains("Right")) {
			GetComponent<Rigidbody2D>().rotation -= 90;
			if (direction==Direction.North) direction=Direction.East;
			else if (direction==Direction.East) direction=Direction.South;
			else if (direction==Direction.South) direction=Direction.West;
			else if (direction==Direction.West) direction=Direction.North;
		}
		else { //uTurn
			GetComponent<Rigidbody2D>().rotation += 180;
			direction=ReverseDirection(direction);
		}
	}

	/// <summary>
	/// Moves in the specified dir for the number ofspaces.
	/// </summary>
	/// <param name="dir">Direction to move in.</param>
	/// <param name="spaces">Number of spaces to move.</param>
	private void move(Direction dir, int spaces) {
		switch (dir) {
		case Direction.South: nextPosition = position + (new Vector2(0f, -1f*spaces));
			StartCoroutine(SmoothMovement(nextPosition));
			position=nextPosition;
			break;
		case Direction.East: nextPosition = position + (new Vector2(1f*spaces, 0f));
			StartCoroutine(SmoothMovement(nextPosition));
			position=nextPosition;
			break;
		case Direction.West: nextPosition = position + (new Vector2(-1f*spaces, 0f));
			StartCoroutine(SmoothMovement(nextPosition));
			position=nextPosition;
			break;
		default: //North
			nextPosition = position + (new Vector2(0f, 1f*spaces));
			StartCoroutine(SmoothMovement(nextPosition));
			position=nextPosition;
			break;
		}
	}

	/// <summary>
	/// Coroutine for smooth movement.
	/// </summary>
	/// <returns>Current state of the movement.</returns>
	/// <param name="end">End position of the movement.</param>
	protected IEnumerator SmoothMovement (Vector3 end)
	{
		while (ongoingMovement) yield return new WaitForSeconds(0.1f);
		ongoingMovement=true;
		//Calculate the remaining distance to move
		float sqrRemainingDistance = (transform.position - end).sqrMagnitude;
		
		while(sqrRemainingDistance > float.Epsilon)
		{
			Vector3 newPostion = Vector3.MoveTowards(GetComponent<Rigidbody2D>().position, end, inverseMoveTime * Time.deltaTime);
			GetComponent<Rigidbody2D>().MovePosition (newPostion);
			sqrRemainingDistance = (transform.position - end).sqrMagnitude;
			yield return null;
		}
		ongoingMovement=false;
	}

	/// <summary>
	/// Checks how far the AI can actually move.
	/// </summary>
	/// <returns>The number of spaces the AI can move unhindered.</returns>
	/// <param name="dir">Direction to move in.</param>
	/// <param name="spaces">Number of intended spaces.</param>
	private int canMoveHowFar(Direction dir, int spaces) {
		switch (dir) {
		case Direction.South: 
			for (int i=0; i<spaces; i++) {
				if (GameManager.instance.getTileType(position + (new Vector2(0f, -i))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2(0f, -i))).Contains("Bottom")) || (GameManager.instance.getTileType(position + (new Vector2(0f, -i))).Contains("D"))) {
						return i;
					}
				}
				else if (GameManager.instance.getTileType(position + (new Vector2(0f, -(i+1)))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2(0f, -(i+1)))).Contains("Top")) || (GameManager.instance.getTileType(position + (new Vector2(0f, -(i+1)))).Contains("U"))) {
						return i;
					}
				}

				if ((GameManager.instance.getTileType(position + (new Vector2(0f, -i))).Contains("Hole")) || (GameManager.instance.getTileType(position + (new Vector2(0f, -i))).Contains("Border"))) {
					return i;
				}
			}
			return spaces;

		case Direction.East:
			for (int i=0; i<spaces; i++) {
				if (GameManager.instance.getTileType(position + (new Vector2(i, 0f))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2(i, 0f))).Contains("Right")) || (GameManager.instance.getTileType(position + (new Vector2(i, 0f))).Contains("R"))) {
						return i;
					}
				}
				else if (GameManager.instance.getTileType(position + (new Vector2((i+1), 0f))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2((i+1), 0f))).Contains("Left")) || (GameManager.instance.getTileType(position + (new Vector2((i+1), 0f))).Contains("LU")) || (GameManager.instance.getTileType(position + (new Vector2((i+1), 0f))).Contains("LD"))) {
						return i;
					}
				}

				if ((GameManager.instance.getTileType(position + (new Vector2(i, 0f))).Contains("Hole")) || (GameManager.instance.getTileType(position + (new Vector2(i, 0f))).Contains("Border"))) {
					return i;
				}
			}
			return spaces;

		case Direction.West: 
			for (int i=0; i<spaces; i++) {
				if (GameManager.instance.getTileType(position + (new Vector2(-i, 0f))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2(-i, 0f))).Contains("Left")) || (GameManager.instance.getTileType(position + (new Vector2(-i, 0f))).Contains("L"))) {
						return i;
					}
				}
				else if (GameManager.instance.getTileType(position + (new Vector2(-(i+1), 0f))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2(-(i+1), 0f))).Contains("Right")) || (GameManager.instance.getTileType(position + (new Vector2(-(i+1), 0f))).Contains("R"))) {
						return i;
					}
				}

				if ((GameManager.instance.getTileType(position + (new Vector2(-i, 0f))).Contains("Hole")) || (GameManager.instance.getTileType(position + (new Vector2(-i, 0f))).Contains("Border"))) {
					return i;
				}
			}
			return spaces;

		default: 
			for (int i=0; i<spaces; i++) {
				if (GameManager.instance.getTileType(position + (new Vector2(0f, i))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2(0f, i))).Contains("Top")) || (GameManager.instance.getTileType(position + (new Vector2(0f, i))).Contains("U"))) {
						return i;
					}
				}
				else if (GameManager.instance.getTileType(position + (new Vector2(0f, (i+1)))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2(0f, (i+1)))).Contains("Bottom")) || (GameManager.instance.getTileType(position + (new Vector2(0f, (i+1)))).Contains("D"))) {
						return i;
					}
				}
				if ((GameManager.instance.getTileType(position + (new Vector2(0f, i))).Contains("Hole")) || (GameManager.instance.getTileType(position + (new Vector2(0f, i))).Contains("Border"))) {
					return i;
				}
			}
			return spaces;
		}
	}

	/// <summary>
	/// Reverses the given direction.
	/// </summary>
	/// <returns>The reverse of the given direction.</returns>
	/// <param name="dir">Direction to reverse.</param>
	Direction ReverseDirection (Direction dir) {
		if(dir == Direction.North) dir = Direction.South;
		else if(dir == Direction.South) dir = Direction.North;
		else if(dir == Direction.East) dir = Direction.West;
		else if(dir == Direction.West) dir = Direction.East;
		return dir;
	}
}
