﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Droppable : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler {

	public bool isHand=true;

	/// <summary>
	/// Moves placeholder to corresponding drop area on pointer enter.
	/// </summary>
	/// <param name="eventData">PointerEnter event data.</param>
	public void OnPointerEnter(PointerEventData eventData) {
		if(eventData.pointerDrag == null)
			return;
		
		Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
		if(d != null) {
			d.placeholderParent = this.transform;
		}
	}

	/// <summary>
	/// Moves placeholder to corresponding drop area on pointer exit.
	/// </summary>
	/// <param name="eventData">PointerExit event data.</param>
	public void OnPointerExit(PointerEventData eventData) {
		if(eventData.pointerDrag == null)
			return;
		
		Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
		if(d != null && d.placeholderParent==this.transform) {
			d.placeholderParent = d.originalParent;
		}
	}

	/// <summary>
	/// Drops card into correct drop area and checks maximum register size of 5.
	/// </summary>
	/// <param name="eventData">OnDrop event data.</param>
	public void OnDrop(PointerEventData eventData) {
		Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
		if(d != null) {
			if (!isHand) {
				if (this.transform.childCount<6)
					d.originalParent = this.transform;
			}
			else d.originalParent = this.transform;
		}
	}
}
