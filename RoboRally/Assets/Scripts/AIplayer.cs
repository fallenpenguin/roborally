﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIplayer : MonoBehaviour {
	
	enum Direction {North, East, South, West};
	
	public static AIplayer aiInst = null;

	public int aiLevel=2;

	public float moveTime = 0.1f;
	private float inverseMoveTime;
	private bool ongoingMovement=false;
	
	private Card[] register;
	private Card[] hand;
	private Vector2 position;
	private Vector2 nextPosition;
	private Vector2 respawn;
	private Vector2 goal;
	private Direction direction;
	private int hp=10;

	//variables for simulation of AI movement
	private Vector2 simPosition;
	private Direction simDirection;
	private bool firstRegOfGame=true;
	private Vector2 tempSimPosition;
	private Direction tempSimDirection;
	
	
	void Awake () {
		if (aiInst == null) aiInst = this;
		else if (aiInst != null) Destroy(gameObject);
		
		inverseMoveTime = 1f / moveTime;
		
		aiLevel=CarrierScript.carrierInst.AiLevel;

		GetComponent<SpriteRenderer>().enabled=true;
		register=new Card[5];
		hand=new Card[9];
		respawn=new Vector2(0f,0f);
		respawnPlayer();
	}

	/// <summary>
	/// Sets the registers for the AI.
	/// </summary>
	public void setRegister() {
		// AI levels: 0 - disabled, 1 - dumbAI, 2 - smartAI
		if (aiLevel!=0) {
			simPosition=position;
			simDirection=direction;
			string[] handStrings = new string[9];
			int[] handWeights = new int[9];
			int[] setCards = new int[5];

			for (int i=0; i<9; i++) {
				handStrings[i]=hand[i].Label;
			}

			for (int fillRegister=0; fillRegister<5; fillRegister++) {
				//find preferred Directions (preferred directions = getting the AI closer to the goal)
				Vector2 prefDirV2=simPosition-goal;
				Direction[] prefDir=new Direction[2];
				if (prefDirV2.x <= 0) prefDir[0]=Direction.East;
				else prefDir[0]=Direction.West;
				if (prefDirV2.y <= 0) prefDir[1]=Direction.North;
				else prefDir[1]=Direction.South;

				//get H value for each card in hand unless 999 (card already used)
				for (int i=0; i<9; i++) {
					if (handWeights[i]!=999) handWeights[i]=getH(handStrings[i], prefDir);
				}

				//choose lowest weight
				int lowest=999;
				int lowestIndex=0;
				for (int i=0; i<9; i++) {
					if (handWeights[i]<lowest) {
						lowestIndex=i;
						lowest=handWeights[i];
					}
				}

				//lock card into register
				register[fillRegister]=hand[lowestIndex];
				setCards[fillRegister]=lowestIndex;
				handWeights[lowestIndex]=999;
				firstRegOfGame=false;

				//update simPos and simDir
				if (handStrings[lowestIndex].Contains("Move")) {
					if (handStrings[lowestIndex].Contains("1")) {
						switch (simDirection) {
						case Direction.South: simPosition = simulateBoardElements(simPosition + (new Vector2(0f, -1f * canMoveHowFar(simDirection, 1))));
							break;
						case Direction.East: simPosition = simulateBoardElements(simPosition + (new Vector2(1f*canMoveHowFar(simDirection, 1), 0f)));
							break;
						case Direction.West: simPosition = simulateBoardElements(simPosition + (new Vector2(-1f*canMoveHowFar(simDirection, 1), 0f)));
							break;
						default: //North
							simPosition = simulateBoardElements(simPosition + (new Vector2(0f, 1f*canMoveHowFar(simDirection, 1))));
							break;
						}
					}
					
					if (handStrings[lowestIndex].Contains("2")) {
						switch (simDirection) {
						case Direction.South: simPosition = simulateBoardElements(simPosition + (new Vector2(0f, -1f * canMoveHowFar(simDirection, 2))));
							break;
						case Direction.East: simPosition = simulateBoardElements(simPosition + (new Vector2(1f*canMoveHowFar(simDirection, 2), 0f)));
							break;
						case Direction.West: simPosition = simulateBoardElements(simPosition + (new Vector2(-1f*canMoveHowFar(simDirection, 2), 0f)));
							break;
						default: //North
							simPosition = simulateBoardElements(simPosition + (new Vector2(0f, 1f*canMoveHowFar(simDirection, 2))));
							break;
						}
					}
					else {
						if (handStrings[lowestIndex].Contains("3")) {
							switch (simDirection) {
							case Direction.South: simPosition = simulateBoardElements(simPosition + (new Vector2(0f, -1f * canMoveHowFar(simDirection, 3))));
								break;
							case Direction.East: simPosition = simulateBoardElements(simPosition + (new Vector2(1f*canMoveHowFar(simDirection, 3), 0f)));
								break;
							case Direction.West: simPosition = simulateBoardElements(simPosition + (new Vector2(-1f*canMoveHowFar(simDirection, 3), 0f)));
								break;
							default: //North
								simPosition = simulateBoardElements(simPosition + (new Vector2(0f, 1f*canMoveHowFar(simDirection, 3))));
								break;
							}
						}
					}
					simDirection=tempSimDirection;
				}
				else if (handStrings[lowestIndex].Contains("Back")) {
					switch (simDirection) {
					case Direction.South: simPosition = simulateBoardElements(simPosition + (new Vector2(0f, -1f * canMoveHowFar(ReverseDirection(simDirection), 1))));
						break;
					case Direction.East: simPosition = simulateBoardElements(simPosition + (new Vector2(1f*canMoveHowFar(ReverseDirection(simDirection), 1), 0f)));
						break;
					case Direction.West: simPosition = simulateBoardElements(simPosition + (new Vector2(-1f*canMoveHowFar(ReverseDirection(simDirection), 1), 0f)));
						break;
					default: //North
						simPosition = simulateBoardElements(simPosition + (new Vector2(0f, 1f*canMoveHowFar(ReverseDirection(simDirection), 1))));
						break;
					}
					simDirection=tempSimDirection;
				}
				//Turns
				else {
					if (handStrings[lowestIndex].Contains("Left")) {
						if (simDirection==Direction.North) simDirection = Direction.West;
						else if (simDirection==Direction.East) simDirection = Direction.North;
						else if (simDirection==Direction.South) simDirection = Direction.East;
						else simDirection = Direction.South; //West
					}
					else if (handStrings[lowestIndex].Contains("Right")) {
						if (simDirection==Direction.North) simDirection = Direction.East;
						else if (simDirection==Direction.East) simDirection = Direction.South;
						else if (simDirection==Direction.South) simDirection = Direction.West;
						else simDirection = Direction.North; //West
					}
					else { //uTurn
						simDirection = ReverseDirection(simDirection);
					}
					simPosition = simulateBoardElements(simPosition);
					simDirection=tempSimDirection;
				}
			}
		}
		else {
			//FOR DEBUGGING
			register[0]=new Card(10,"Move3");
			register[1]=new Card(10,"Move3");
			register[2]=new Card(10,"Move3");
			register[3]=new Card(10,"Right");
			register[4]=new Card(10,"Move3");
		}
	}

	/// <summary>
	/// Simulates AI movement without actually moving the AI robot.
	/// </summary>
	/// <returns>The new coordinates the AI would have after the move including possible board elements.</returns>
	/// <param name="dir">Direction in which to move.</param>
	/// <param name="spaces">Number of spaces to move.</param>
	private Vector3 simulateMove(Direction dir, int spaces) {
		switch (dir) {
		case Direction.South: return simulateBoardElements(simPosition + (new Vector2(0f, -1f*spaces)));
		case Direction.East: return simulateBoardElements(simPosition + (new Vector2(1f*spaces, 0f)));
		case Direction.West: return simulateBoardElements(simPosition + (new Vector2(-1f*spaces, 0f)));
		default: //North
			return simulateBoardElements(simPosition + (new Vector2(0f, 1f*spaces)));
		}
	}

	/// <summary>
	/// Simulates AI rotation without actually turning the AI robot.
	/// </summary>
	/// <returns>The new rotation the AI would have after the turn including possible board elements.</returns>
	/// <param name="turnDir">Direction in which to turn.</param>
	private Direction simulateTurn(string turnDir) {
		Direction returnDir;

		if (turnDir.Contains("Left")) {
			if (simDirection==Direction.North) returnDir = Direction.West;
			else if (simDirection==Direction.East) returnDir = Direction.North;
			else if (simDirection==Direction.South) returnDir = Direction.East;
			else returnDir = Direction.South; //West
		}
		else if (turnDir.Contains("Right")) {
			if (simDirection==Direction.North) returnDir = Direction.East;
			else if (simDirection==Direction.East) returnDir = Direction.South;
			else if (simDirection==Direction.South) returnDir = Direction.West;
			else returnDir = Direction.North; //West
		}
		else { //uTurn
			returnDir = ReverseDirection(simDirection);
		}
		tempSimPosition=simulateBoardElements(simPosition);
		return returnDir;
	}

	/// <summary>
	/// Determines the H value for the current card.
	/// </summary>
	/// <returns>The H value.</returns>
	/// <param name="card">Name of the current card.</param>
	/// <param name="prefs">Array of preferred directions.</param>
	private int getH(string card, Direction[] prefs) {
		int finalH=0;
		int currentH=0;
		bool didMove=false;
		bool didTurn=false;
		Vector3 simulatedPosition=simPosition;
		Direction simulatedDirection=simDirection;

		//simulate moves
		if (card.Contains("Move")) {
			if (card.Contains("1")) {
				if (firstRegOfGame) return 2;
				if (canMoveHowFar(direction, 1)==0) return 900;
				simulatedPosition=simulateMove(simulatedDirection, canMoveHowFar(direction, 1));
				didMove=true;
			}
			
			if (card.Contains("2")) {
				if (firstRegOfGame) return 1;
				if (canMoveHowFar(direction, 2)==0) return 900;
				simulatedPosition=simulateMove(simulatedDirection, canMoveHowFar(direction, 2));
				didMove=true;
			}
			else {
				if (card.Contains("3")) {
					if (firstRegOfGame) return 0;
					if (canMoveHowFar(direction, 3)==0) return 900;
					simulatedPosition=simulateMove(simulatedDirection, canMoveHowFar(direction, 3));
					didMove=true;
				}
			}
		}
		else if (card.Contains("Back")) {
			simulatedPosition=simulateMove(ReverseDirection(simulatedDirection),1);
			didMove=true;
		}
		//Turns
		else {
			simulatedDirection=simulateTurn(card);
			didTurn=true;
		}

		if (!didMove) simulatedPosition=tempSimPosition;
		if (!didTurn) simulatedDirection=tempSimDirection;


		//calculate finalH, Manhattan distance + turn coefficient
		int finalHpartA = (int) (simulatedPosition.x-goal.x);
		if (finalHpartA<0) finalHpartA=finalHpartA*-1;
		int finalHpartB = (int) (simulatedPosition.y-goal.y);
		if (finalHpartB<0) finalHpartB=finalHpartB*-1;
		finalH = finalHpartA + finalHpartB;

		int currentHpartA = (int) (simPosition.x-goal.x);
		if (currentHpartA<0) currentHpartA=currentHpartA*-1;
		int currentHpartB = (int) (simPosition.y-goal.y);
		if (currentHpartB<0) currentHpartB=currentHpartB*-1;
		currentH = currentHpartA + currentHpartB;

		if ((simDirection!=prefs[0]) && (simDirection!=prefs[1])) {
			if ((simulatedDirection==prefs[0]) || (simulatedDirection==prefs[1])) {
				return finalH+5;
			}
			else return finalH+10;
		}
		else {
			if (finalH < currentH) return finalH;
			else if ((simulatedDirection==prefs[0]) || (simulatedDirection==prefs[1])) return finalH+5;
			else return finalH+10;
		}
	}

	/// <summary>
	/// Simulates the board elements withour actually applying their movement and rotation to the AI robot.
	/// </summary>
	/// <returns>The position of the AI after board element application.</returns>
	/// <param name="simulatedMoveLocation">Simulated current position.</param>
	private Vector2 simulateBoardElements(Vector2 simulatedMoveLocation) {
		tempSimDirection=simDirection;
		tempSimPosition=simPosition;

		//AI level 2: smartAI takes board elements into consideration, level 1: dumb AI does not
		if (aiLevel==2) {
			string currentTileType=GameManager.instance.getTileType(simulatedMoveLocation);

			//check hole or borders
			if ((currentTileType.Contains("Hole")) || (currentTileType.Contains("Border"))) {
				tempSimPosition = respawn;
			}

			//blue conveyors move
			if (currentTileType.Contains("BConv")) {
				if ((currentTileType.Contains("Left")) || (currentTileType.Contains("DL")) || (currentTileType.Contains("UL"))) {
					simulatedMoveLocation = simulatedMoveLocation + (new Vector2(-1f, 0f));
				}
				else if ((currentTileType.Contains("Right")) || (currentTileType.Contains("DR")) || (currentTileType.Contains("UR"))) {
					simulatedMoveLocation = simulatedMoveLocation + (new Vector2(1f, 0f));
				}
				else if ((currentTileType.Contains("Up")) || (currentTileType.Contains("LU")) || (currentTileType.Contains("RU"))) {
					simulatedMoveLocation = simulatedMoveLocation + (new Vector2(0f, 1f));
				}
				else if ((currentTileType.Contains("Down")) || (currentTileType.Contains("LD")) || (currentTileType.Contains("RD"))) {
					simulatedMoveLocation = simulatedMoveLocation + (new Vector2(0f, -1f));
				}

				//check for rotation
				currentTileType=GameManager.instance.getTileType(simulatedMoveLocation);
				if ((currentTileType.Contains("DL")) || (currentTileType.Contains("LU")) || (currentTileType.Contains("RD")) || (currentTileType.Contains("UR"))) {
					if (tempSimDirection==Direction.North) tempSimDirection = Direction.West;
					else if (tempSimDirection==Direction.East) tempSimDirection = Direction.North;
					else if (tempSimDirection==Direction.South) tempSimDirection = Direction.East;
					else tempSimDirection = Direction.South; //West
				}
				else if ((currentTileType.Contains("DR")) || (currentTileType.Contains("LD")) || (currentTileType.Contains("RU")) || (currentTileType.Contains("UL"))) {
					if (tempSimDirection==Direction.North) tempSimDirection = Direction.East;
					else if (tempSimDirection==Direction.East) tempSimDirection = Direction.South;
					else if (tempSimDirection==Direction.South) tempSimDirection = Direction.West;
					else tempSimDirection = Direction.North; //West
				}
			}
			
			//all conveyors move
			if (currentTileType.Contains("Conv")) {
				if ((currentTileType.Contains("Left")) || (currentTileType.Contains("DL")) || (currentTileType.Contains("UL"))) {
					simulatedMoveLocation = simulatedMoveLocation + (new Vector2(-1f, 0f));
				}
				else if ((currentTileType.Contains("Right")) || (currentTileType.Contains("DR")) || (currentTileType.Contains("UR"))) {
					simulatedMoveLocation = simulatedMoveLocation + (new Vector2(1f, 0f));
				}
				else if ((currentTileType.Contains("Up")) || (currentTileType.Contains("LU")) || (currentTileType.Contains("RU"))) {
					simulatedMoveLocation = simulatedMoveLocation + (new Vector2(0f, 1f));
				}
				else if ((currentTileType.Contains("Down")) || (currentTileType.Contains("LD")) || (currentTileType.Contains("RD"))) {
					simulatedMoveLocation = simulatedMoveLocation + (new Vector2(0f, -1f));
				}

				//check for rotation
				currentTileType=GameManager.instance.getTileType(simulatedMoveLocation);
				if ((currentTileType.Contains("DL")) || (currentTileType.Contains("LU")) || (currentTileType.Contains("RD")) || (currentTileType.Contains("UR"))) {
					if (tempSimDirection==Direction.North) tempSimDirection = Direction.West;
					else if (tempSimDirection==Direction.East) tempSimDirection = Direction.North;
					else if (tempSimDirection==Direction.South) tempSimDirection = Direction.East;
					else tempSimDirection = Direction.South; //West
				}
				else if ((currentTileType.Contains("DR")) || (currentTileType.Contains("LD")) || (currentTileType.Contains("RU")) || (currentTileType.Contains("UL"))) {
					if (tempSimDirection==Direction.North) tempSimDirection = Direction.East;
					else if (tempSimDirection==Direction.East) tempSimDirection = Direction.South;
					else if (tempSimDirection==Direction.South) tempSimDirection = Direction.West;
					else tempSimDirection = Direction.North; //West
				}
			}

			//gears turn
			if (currentTileType.Contains("Gear")) {
				if (currentTileType.Contains("Left")) {
					if (tempSimDirection==Direction.North) tempSimDirection = Direction.West;
					else if (tempSimDirection==Direction.East) tempSimDirection = Direction.North;
					else if (tempSimDirection==Direction.South) tempSimDirection = Direction.East;
					else tempSimDirection = Direction.South; //West
				}
				else if (currentTileType.Contains("Right")) {
					if (tempSimDirection==Direction.North) tempSimDirection = Direction.East;
					else if (tempSimDirection==Direction.East) tempSimDirection = Direction.South;
					else if (tempSimDirection==Direction.South) tempSimDirection = Direction.West;
					else tempSimDirection = Direction.North; //West
				}
			}
			
			//lasers fire
			if (currentTileType.Contains("Laser")) {
				if ((hp-1)==0) {
					tempSimPosition = respawn;
				}
			}
			
			//repairs not taken into consideration
		}
		return simulatedMoveLocation;
	}

	/// <summary>
	/// Respawns the player at set spawn location.
	/// </summary>
	public void respawnPlayer() {
		position=respawn;
		nextPosition=respawn;
		hp=10;
		direction=Direction.North;
		GetComponent<Rigidbody2D>().rotation=90;
		StartCoroutine(SmoothMovement(respawn));
	}

	/// <summary>
	/// Sets the respawn location.
	/// </summary>
	/// <param name="location">Coordinates of respawn location.</param>
	public void setRespawn(Vector2 location) {
		respawn=location;
	}

	/// <summary>
	/// Sets the goal for the AI (checkpoint location).
	/// </summary>
	/// <param name="location">Coordinates of the checkpoint.</param>
	public void setGoal(Vector2 location) {
		goal=location;
	}

	/// <summary>
	/// Sets the hand cards for the AI.
	/// </summary>
	/// <param name="index">Index of card to be set.</param>
	/// <param name="card">Card supposed to be set into the index.</param>
	public void setHand( int index, Card card) {
		if (index < 9) hand[index]=card;
		else Debug.Log("setHand index too high: "+index);
	}

	/// <summary>
	/// Gets the hand cards for the AI.
	/// </summary>
	/// <returns>The card found at the specified index in hand.</returns>
	/// <param name="index">Index of card to get.</param>
	public Card getHand(int index) {
		if (index < 9) return hand[index];
		else {
			Debug.Log("Returned index 0, getHand index too high: "+index);
			return hand[0];
		}
	}

	/// <summary>
	/// Checks to see if AI won.
	/// </summary>
	public bool wins() {
		if (GameManager.instance.getTileType(position).Contains("Checkpoint")) return true;
		return false;
		
	}

	/// <summary>
	/// Gets the register priority.
	/// </summary>
	/// <returns>The register priority.</returns>
	/// <param name="number">Index of register to check.</param>
	public int getRegisterPriority(int number) {
		return register[number].Priority;
	}
	
	/// <summary>
	/// Executes the specified player register and board elements.
	/// </summary>
	/// <returns><c>true</c>, if player survived, <c>false</c> if player died.</returns>
	public bool executeRegister(int number) {
		if (register[number].Label.Contains("Move")) {
			if (register[number].Label.Contains("1")) {
				move(direction, canMoveHowFar(direction, 1));
			}

			if (register[number].Label.Contains("2")) {
				move(direction, canMoveHowFar(direction, 2));
			}
			else {
				if (register[number].Label.Contains("3")) {
					move(direction, canMoveHowFar(direction, 3));
				}
			}
		}
		else if (register[number].Label.Contains("Back")) {
			move(ReverseDirection(direction), canMoveHowFar(ReverseDirection(direction), 1));	
		}
		//Turns
		else turn(register[number].Label);

		return doBoardElements();
	}

	/// <summary>
	/// Apply board elements to AI robot.
	/// </summary>
	/// <returns><c>true</c>, if player survived, <c>false</c> if player died.</returns>
	private bool doBoardElements() {
		string currentTileType=GameManager.instance.getTileType(position);
				
		//check hole or borders
		if ((currentTileType.Contains("Hole")) || (currentTileType.Contains("Border"))) {
			respawnPlayer();
			return false;
		}
		
		//blue conveyors move
		if (currentTileType.Contains("BConv")) {
			if ((currentTileType.Contains("Left")) || (currentTileType.Contains("DL")) || (currentTileType.Contains("UL"))) {
				move(Direction.West, 1);
			}
			else if ((currentTileType.Contains("Right")) || (currentTileType.Contains("DR")) || (currentTileType.Contains("UR"))) {
				move(Direction.East, 1);
			}
			else if ((currentTileType.Contains("Up")) || (currentTileType.Contains("LU")) || (currentTileType.Contains("RU"))) {
				move(Direction.North, 1);
			}
			else if ((currentTileType.Contains("Down")) || (currentTileType.Contains("LD")) || (currentTileType.Contains("RD"))) {
				move(Direction.South, 1);
			}
			
			currentTileType=GameManager.instance.getTileType(position);
			if ((currentTileType.Contains("DL")) || (currentTileType.Contains("LU")) || (currentTileType.Contains("RD")) || (currentTileType.Contains("UR"))) {
				turn("Left");
			}
			else if ((currentTileType.Contains("DR")) || (currentTileType.Contains("LD")) || (currentTileType.Contains("RU")) || (currentTileType.Contains("UL"))) {
				turn("Right");
			}
		}
		
		//all conveyors move
		if (currentTileType.Contains("Conv")) {
			if ((currentTileType.Contains("Left")) || (currentTileType.Contains("DL")) || (currentTileType.Contains("UL"))) {
				move(Direction.West, 1);
			}
			else if ((currentTileType.Contains("Right")) || (currentTileType.Contains("DR")) || (currentTileType.Contains("UR"))) {
				move(Direction.East, 1);
			}
			else if ((currentTileType.Contains("Up")) || (currentTileType.Contains("LU")) || (currentTileType.Contains("RU"))) {
				move(Direction.North, 1);
			}
			else if ((currentTileType.Contains("Down")) || (currentTileType.Contains("LD")) || (currentTileType.Contains("RD"))) {
				move(Direction.South, 1);
			}
			
			currentTileType=GameManager.instance.getTileType(position);
			if ((currentTileType.Contains("DL")) || (currentTileType.Contains("LU")) || (currentTileType.Contains("RD")) || (currentTileType.Contains("UR"))) {
				turn("Left");
			}
			else if ((currentTileType.Contains("DR")) || (currentTileType.Contains("LD")) || (currentTileType.Contains("RU")) || (currentTileType.Contains("UL"))) {
				turn("Right");
			}
		}
		
		//gears turn
		if (currentTileType.Contains("Gear")) {
			if (currentTileType.Contains("Left")) {
				turn("Left");
			}
			else if (currentTileType.Contains("Right")) {
				turn("Right");
			}
		}
		
		//lasers fire
		if (currentTileType.Contains("Laser")) {
			hp--;
			if (hp==0) {
				respawnPlayer();
				return false;
			}
		}
		
		//repairs
		if (currentTileType.Contains("Wrench")) {
			if (hp<10) hp++;
		}
		
		return true;
	}

	/// <summary>
	/// Turn the AI robot in specified direction.
	/// </summary>
	/// <param name="turnDir">Direction to turn to.</param>
	private void turn(string turnDir) {
		if (turnDir.Contains("Left")) {
			GetComponent<Rigidbody2D>().rotation += 90;
			if (direction==Direction.North) direction=Direction.West;
			else if (direction==Direction.East) direction=Direction.North;
			else if (direction==Direction.South) direction=Direction.East;
			else if (direction==Direction.West) direction=Direction.South;
		}
		else if (turnDir.Contains("Right")) {
			GetComponent<Rigidbody2D>().rotation -= 90;
			if (direction==Direction.North) direction=Direction.East;
			else if (direction==Direction.East) direction=Direction.South;
			else if (direction==Direction.South) direction=Direction.West;
			else if (direction==Direction.West) direction=Direction.North;
		}
		else { //uTurn
			GetComponent<Rigidbody2D>().rotation += 180;
			direction=ReverseDirection(direction);
		}
	}

	/// <summary>
	/// Moves the AI robot in specified direction and the appropriate number of spaces.
	/// </summary>
	/// <param name="dir">Direction to move.</param>
	/// <param name="spaces">Number of spaces to move.</param>
	private void move(Direction dir, int spaces) {
		switch (dir) {
		case Direction.South: nextPosition = position + (new Vector2(0f, -1f*spaces));
			StartCoroutine(SmoothMovement(nextPosition));
			position=nextPosition;
			break;
		case Direction.East: nextPosition = position + (new Vector2(1f*spaces, 0f));
			StartCoroutine(SmoothMovement(nextPosition));
			position=nextPosition;
			break;
		case Direction.West: nextPosition = position + (new Vector2(-1f*spaces, 0f));
			StartCoroutine(SmoothMovement(nextPosition));
			position=nextPosition;
			break;
		default: //North
			nextPosition = position + (new Vector2(0f, 1f*spaces));
			StartCoroutine(SmoothMovement(nextPosition));
			position=nextPosition;
			break;
		}
	}
	
	/// <summary>
	/// Coroutine for smooth movement.
	/// </summary>
	/// <returns>Current state of the movement.</returns>
	/// <param name="end">End position of the movement.</param>
	protected IEnumerator SmoothMovement (Vector3 end)
	{
		while (ongoingMovement) yield return new WaitForSeconds(0.1f);
		ongoingMovement=true;
		//Calculate the remaining distance to move
		float sqrRemainingDistance = (transform.position - end).sqrMagnitude;
		
		while(sqrRemainingDistance > float.Epsilon)
		{
			Vector3 newPostion = Vector3.MoveTowards(GetComponent<Rigidbody2D>().position, end, inverseMoveTime * Time.deltaTime);
			GetComponent<Rigidbody2D>().MovePosition (newPostion);
			sqrRemainingDistance = (transform.position - end).sqrMagnitude;
			yield return null;
		}
		ongoingMovement=false;
	}
	
	/// <summary>
	/// Checks how far the AI can actually move.
	/// </summary>
	/// <returns>The number of spaces the AI can move unhindered.</returns>
	/// <param name="dir">Direction to move in.</param>
	/// <param name="spaces">Number of intended spaces.</param>
	private int canMoveHowFar(Direction dir, int spaces) {
		switch (dir) {
		case Direction.South: 
			for (int i=0; i<spaces; i++) {
				if (GameManager.instance.getTileType(position + (new Vector2(0f, -i))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2(0f, -i))).Contains("Bottom")) || (GameManager.instance.getTileType(position + (new Vector2(0f, -i))).Contains("D"))) {
						return i;
					}
				}
				else if (GameManager.instance.getTileType(position + (new Vector2(0f, -(i+1)))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2(0f, -(i+1)))).Contains("Top")) || (GameManager.instance.getTileType(position + (new Vector2(0f, -(i+1)))).Contains("U"))) {
						return i;
					}
				}
				if ((GameManager.instance.getTileType(position + (new Vector2(0f, -i))).Contains("Hole")) || (GameManager.instance.getTileType(position + (new Vector2(0f, -i))).Contains("Border"))) {
					return i;
				}
			}
			return spaces;

		case Direction.East:
			for (int i=0; i<spaces; i++) {
				if (GameManager.instance.getTileType(position + (new Vector2(i, 0f))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2(i, 0f))).Contains("Right")) || (GameManager.instance.getTileType(position + (new Vector2(i, 0f))).Contains("R"))) {
						return i;
					}
				}
				else if (GameManager.instance.getTileType(position + (new Vector2((i+1), 0f))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2((i+1), 0f))).Contains("Left")) || (GameManager.instance.getTileType(position + (new Vector2((i+1), 0f))).Contains("LU")) || (GameManager.instance.getTileType(position + (new Vector2((i+1), 0f))).Contains("LD"))) {
						return i;
					}
				}
				if ((GameManager.instance.getTileType(position + (new Vector2(i, 0f))).Contains("Hole")) || (GameManager.instance.getTileType(position + (new Vector2(i, 0f))).Contains("Border"))) {
					return i;
				}
			}
			return spaces;

		case Direction.West: 
			for (int i=0; i<spaces; i++) {
				if (GameManager.instance.getTileType(position + (new Vector2(-i, 0f))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2(-i, 0f))).Contains("Left")) || (GameManager.instance.getTileType(position + (new Vector2(-i, 0f))).Contains("L"))) {
						return i;
					}
				}
				else if (GameManager.instance.getTileType(position + (new Vector2(-(i+1), 0f))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2(-(i+1), 0f))).Contains("Right")) || (GameManager.instance.getTileType(position + (new Vector2(-(i+1), 0f))).Contains("R"))) {
						return i;
					}
				}
				if ((GameManager.instance.getTileType(position + (new Vector2(-i, 0f))).Contains("Hole")) || (GameManager.instance.getTileType(position + (new Vector2(-i, 0f))).Contains("Border"))) {
					return i;
				}
			}
			return spaces;

		default: 
			for (int i=0; i<spaces; i++) {
				if (GameManager.instance.getTileType(position + (new Vector2(0f, i))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2(0f, i))).Contains("Top")) || (GameManager.instance.getTileType(position + (new Vector2(0f, i))).Contains("U"))) {
						return i;
					}
				}
				else if (GameManager.instance.getTileType(position + (new Vector2(0f, (i+1)))).Contains("Wall")) {
					if ((GameManager.instance.getTileType(position + (new Vector2(0f, (i+1)))).Contains("Bottom")) || (GameManager.instance.getTileType(position + (new Vector2(0f, (i+1)))).Contains("D"))) {
						return i;
					}
				}
				if ((GameManager.instance.getTileType(position + (new Vector2(0f, i))).Contains("Hole")) || (GameManager.instance.getTileType(position + (new Vector2(0f, i))).Contains("Border"))) {
					return i;
				}
			}
			return spaces;
		}
	}

	/// <summary>
	/// Reverses the given direction.
	/// </summary>
	/// <returns>The reverse of the given direction.</returns>
	/// <param name="dir">Direction to reverse.</param>
	Direction ReverseDirection (Direction dir) {
		if(dir == Direction.North) dir = Direction.South;
		else if(dir == Direction.South) dir = Direction.North;
		else if(dir == Direction.East) dir = Direction.West;
		else if(dir == Direction.West) dir = Direction.East;
		return dir;
	}
}
