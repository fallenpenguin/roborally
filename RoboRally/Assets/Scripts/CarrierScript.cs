﻿using UnityEngine;
using System.Collections;

public class CarrierScript : MonoBehaviour {

	public static CarrierScript carrierInst = null;

	private int aiLevel;
	
	public int AiLevel { get { return aiLevel; } set { aiLevel=value; } }

	void Awake() {
		if (carrierInst == null) carrierInst = this;
		else if (carrierInst != null) Destroy(gameObject);
		DontDestroyOnLoad(gameObject);
	}
}
