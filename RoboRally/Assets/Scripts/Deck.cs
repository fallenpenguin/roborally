﻿using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public struct Card {
	private int priority;
	private string label;

	public Card(int prio, string name) {
		priority=prio;
		label=name;
	}

	public string Label { get { return label; } }
	public int Priority { get { return priority; } }
}

public class Deck : MonoBehaviour {

	private int move3Count=6;
	private int move2Count=12;
	private int move1Count=18;
	private int backCount=6;
	private int leftCount=18;
	private int rightCount=18;
	private int uCount=6;
	
	private int[] move3Prio={840,830,820,810,800,790};
	private int[] move2Prio={780,770,760,750,740,730,720,710,700,690,680,670};
	private int[] move1Prio={660,650,640,630,620,610,600,590,580,570,560,550,540,530,520,510,500,490};
	private int[] backPrio={480,470,460,450,440,430};
	private int[] leftPrio={410,390,370,350,330,310,290,270,250,230,210,190,170,150,130,110,90,70};
	private int[] rightPrio={420,400,380,360,340,320,300,280,260,240,220,200,180,160,140,120,100,80};
	private int[] uPrio={60,50,40,30,20,10};

	//actual deck of cards
	private List <Card> deck = new List<Card>();
	//holding area for cards currently in hand
	private List <Card> holding = new List<Card>();
	//discard pile
	private List <Card> discard = new List<Card>();

	/// <summary>
	/// Initializes the deck.
	/// </summary>
	public void initDeck() {
		resetDeck();
	}

	/// <summary>
	/// Draws a card.
	/// </summary>
	/// <returns>The card that has been drawn from the deck.</returns>
	public Card drawCard() {
		if (deck.Count==0) {
			for (int i=0; i<discard.Count; i++) deck.Add(discard[i]);
			shuffleDeck();
			discard.Clear();
		}
		Card temp=deck[0];
		deck.RemoveAt(0);
		holding.Add(temp);
		if (holding.Count==(GameManager.instance.playerNumber*9)) {
			for (int i=0; i<holding.Count; i++) discard.Add(holding[i]);
			holding.Clear();
		}
		return temp;
	}

	/** FOR DEBUG**/
	/// <summary>
	/// Writes every card in the deck to the console for testing.
	/// </summary>
	public void checkDeck() {
		for (int i = 0; i < deck.Count; i++) {
			Debug.Log(deck[i].Label+" "+deck[i].Priority);
		}
	}

	/// <summary>
	/// Shuffles the deck.
	/// </summary>
	private void shuffleDeck() {
		for (int i = 0; i < deck.Count; i++) {
			Card temp = deck[i];
			int randomIndex = Random.Range(i, deck.Count);
			deck[i] = deck[randomIndex];
			deck[randomIndex] = temp;
		}
	}

	/// <summary>
	/// Resets and shuffles the deck.
	/// </summary>
	public void resetDeck() {
		deck.Clear();
		holding.Clear();
		discard.Clear();

		for (int i=0; i<move3Count; i++) deck.Add(new Card(move3Prio[i], "Move3"));

		for (int i=0; i<move2Count; i++) deck.Add(new Card(move2Prio[i], "Move2"));

		for (int i=0; i<move1Count; i++) deck.Add(new Card(move1Prio[i], "Move1"));

		for (int i=0; i<backCount; i++) deck.Add(new Card(backPrio[i], "Back"));

		for (int i=0; i<leftCount; i++) deck.Add(new Card(leftPrio[i], "Left"));

		for (int i=0; i<rightCount; i++) deck.Add(new Card(rightPrio[i], "Right"));

		for (int i=0; i<uCount; i++) deck.Add(new Card(uPrio[i], "uTurn"));

		shuffleDeck();
	}
}
