﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null;

	public int playerNumber=2;
	public int playerSpawnLocation=1;
	public int aiSpawnLocation=2;
	public Sprite[] sprites;

	public GameObject handHolder;
	public GameObject registerHolder;
	public GameObject buttonHolder;
	public GameObject notificationHolder;
	public GameObject quitButtonHolder;
	public GameObject cardTemplate;

	private BoardManager boardScript;
	private Deck deckScript;
	private int level = 1;
	private bool pauseGameLoop=false;


	void Awake () {
		if (instance == null) instance = this;
		else if (instance != null) Destroy(gameObject);
		DontDestroyOnLoad(gameObject);

		boardScript=GetComponent<BoardManager>();
		deckScript=GetComponent<Deck>();

		handHolder = GameObject.Find("Hand");
		registerHolder = GameObject.Find("Registers");
		buttonHolder = GameObject.Find("lockButton");
		notificationHolder = GameObject.Find("Notification");
		quitButtonHolder = GameObject.Find("quitWinButton");

		InitGame();
	}

	/// <summary>
	/// Initializes the game.
	/// </summary>
	void InitGame() {
		boardScript.SetupScene(level);
		deckScript.initDeck();

		Player.playerInst.setRespawn(boardScript.getSpawn(playerSpawnLocation));
		Player.playerInst.respawnPlayer();

		AIplayer.aiInst.setRespawn(boardScript.getSpawn(aiSpawnLocation));
		AIplayer.aiInst.setGoal(boardScript.getCheckpoint());
		AIplayer.aiInst.respawnPlayer();

		handHolder.SetActive(false);
		registerHolder.SetActive(false);
		buttonHolder.SetActive(false);
		notificationHolder.SetActive(false);
		quitButtonHolder.SetActive(false);

		//turn loop
		StartCoroutine("gameLoop");


	}

	/// <summary>
	/// The actual game loop containing the turn structure.
	/// </summary>
	/// <returns>Current loop status.</returns>
	IEnumerator gameLoop() {
		while ((!Player.playerInst.wins()) || (!AIplayer.aiInst.wins())) {
			drawHand("Player");
			drawHand("AI");

			//AI calculations
			AIplayer.aiInst.setRegister();

			//show Interface to player and pause loop until player locks in their cards
			showInterface();
			while (pauseGameLoop)
			{
				yield return new WaitForFixedUpdate();
			}

			//execute registers
			for (int i=0; i<5; i++) {
				if (Player.playerInst.getRegisterPriority(i) > AIplayer.aiInst.getRegisterPriority(i)) {
					if (!Player.playerInst.executeRegister(i)) showError(2);
					if (!AIplayer.aiInst.executeRegister(i)) showError(3);
				}
				else {
					if (!AIplayer.aiInst.executeRegister(i)) showError(3);
					if (!Player.playerInst.executeRegister(i)) showError(2);
				}

				if ((Player.playerInst.wins()) || (AIplayer.aiInst.wins())) break;

				yield return new WaitForSeconds(1f);
			}
			if ((Player.playerInst.wins()) || (AIplayer.aiInst.wins())) break;
			yield return null;
		}
		notificationHolder.SetActive(true);
		if (Player.playerInst.wins()) notificationHolder.GetComponent<Text>().text="You Win.";
		else notificationHolder.GetComponent<Text>().text="AI Win.";
		quitButtonHolder.SetActive(true);

	}

	/// <summary>
	/// Quits the game.
	/// </summary>
	public void quitGame() {
		Application.Quit();
	}

	///<summary>
	/// Shows error message.
	/// 1 - less than 5 cards in registers
	/// 2 - player died and respawned
	/// 3 - ai died and respawned
	///</summary>
	public void showError(int errorCode) {
		notificationHolder.SetActive(true);

		switch (errorCode) {
		case 1: notificationHolder.GetComponent<Text>().text="You need exactly 5 programmed moves.";
			break;
		case 2: notificationHolder.GetComponent<Text>().text="You died and respawned.";
			break;
		case 3: notificationHolder.GetComponent<Text>().text="AI died and respawned.";
			break;
		default: notificationHolder.GetComponent<Text>().text="An error has occured.";
			break;
		}
		Invoke("hideError", 3f);
	}

	/// <summary>
	/// Hides the error message.
	/// </summary>
	private void hideError()
	{
		notificationHolder.SetActive(false);
	}

	/// <summary>
	/// Sets player register to cards locked in the interface.
	/// </summary>
	public void onButtonClick() {
		if (registerHolder.transform.childCount==5) {
			for (int i = registerHolder.transform.childCount - 1; i >= 0; i--)
			{
				GameObject temp = registerHolder.transform.GetChild(i).gameObject;
				Image[] images=temp.GetComponentsInChildren<Image>();
				int tempPrio;
				int.TryParse(temp.GetComponentInChildren<Text>().text, out tempPrio);
				string tempString=images[1].sprite.ToString().Substring(5);
				Player.playerInst.setRegister(i, new Card(tempPrio, tempString));
			}
			hideInterface();
		}
		else {
			showError(1);
		}
	}

	/// <summary>
	/// Hides the interface.
	/// </summary>
	private void hideInterface()
	{
		handHolder.SetActive(false);
		registerHolder.SetActive(false);
		buttonHolder.SetActive(false);
		pauseGameLoop=false;
	}

	/// <summary>
	/// Shows and fills the interface.
	/// </summary>
	private void showInterface() {
		handHolder.SetActive(true);
		registerHolder.SetActive(true);
		buttonHolder.SetActive(true);
		pauseGameLoop=true;

		//clear Hand
		for (int i = handHolder.transform.childCount - 1; i >= 0; i--)
		{
			Transform tempTransform = handHolder.transform.GetChild(i);
			tempTransform.transform.SetParent(null);
			Destroy(tempTransform.gameObject);
		}

		//clear Registers
		for (int i = registerHolder.transform.childCount - 1; i >= 0; i--)
		{
			Transform tempTransform = registerHolder.transform.GetChild(i);
			tempTransform.transform.SetParent(null);
			Destroy(tempTransform.gameObject);
		}
		
		//instantiate new cards
		for (int i=0; i<9; i++) {
			GameObject instance = Instantiate (cardTemplate) as GameObject;
			instance.transform.SetParent(handHolder.transform, false);


			Card tempCard=Player.playerInst.getHand(i);
			Image[] images=instance.GetComponentsInChildren<Image>();
			//Debug.Log(tempCard.Label);
			switch(tempCard.Label) {
			case "Move3": images[1].sprite=sprites[0];
				break;
			case "Move2": images[1].sprite=sprites[1];
				break;
			case "Move1": images[1].sprite=sprites[2];
				break;
			case "Back": images[1].sprite=sprites[3];
				break;
			case "Left": images[1].sprite=sprites[4];
				break;
			case "Right": images[1].sprite=sprites[5];
				break;
			default: //uTurn
				images[1].sprite=sprites[6];
				break;
			}

			instance.GetComponentInChildren<Text>().text=tempCard.Priority.ToString();
		}
	}

	/// <summary>
	/// Draws a full hand of cards for the specified player.
	/// </summary>
	/// <param name="who">Player to draw a hand of cards.</param>
	private void drawHand(string who) {
		if (who=="Player") {
			for (int i=0; i<9; i++)
				Player.playerInst.setHand(i,deckScript.drawCard());
		}
		else if (who=="AI") {
			for (int i=0; i<9; i++)
				AIplayer.aiInst.setHand(i,deckScript.drawCard());
		}
	}

	/// <summary>
	/// Gets the type of the tile.
	/// </summary>
	/// <returns>The tile type.</returns>
	/// <param name="pos">Coordinates of tile to ckeck.</param>
	public string getTileType(Vector2 pos) {
		return boardScript.getTileType(pos);
	}
}
