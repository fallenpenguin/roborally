﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour {

	public int columns = 14;
	public int rows = 18;
	public GameObject[] tiles;

	private int stage;
	private int[] stage1={33,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,31,28,0,73,0,0,0,0,54,0,54,0,0,54,0,54,0,0,27,28,0,70,0,0,0,11,1,1,6,0,0,11,1,1,6,0,27,28,53,0,0,52,53,3,0,39,4,36,0,3,60,39,4,52,27,28,0,67,0,0,0,3,39,61,4,43,51,13,41,53,4,0,27,28,53,0,0,52,53,7,2,2,10,0,36,7,2,2,10,52,27,28,0,63,0,0,0,0,0,44,36,0,0,0,48,36,0,0,27,28,0,65,0,0,0,0,36,49,0,0,0,36,45,0,0,0,27,28,53,54,0,52,53,11,1,1,6,36,0,11,1,1,6,52,27,28,0,68,0,0,0,3,52,41,14,51,42,3,61,39,4,0,27,28,53,54,0,52,53,3,39,60,4,0,36,3,39,0,4,52,27,28,0,71,0,0,0,7,2,2,10,0,0,7,2,2,10,0,27,28,0,75,0,0,0,0,55,0,55,0,0,55,0,55,76,0,27,34,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,32};

	private Transform boardHolder;
	private List <Vector3> gridPositions = new List <Vector3> ();

	/// <summary>
	/// Initialises the list of possible coordinates.
	/// </summary>
	void InitialiseList () {
		gridPositions.Clear ();
		
		//columns
		for(int x = 0; x < columns; x++) {
			//rows
			for(int y = 0; y < rows; y++) {
				gridPositions.Add (new Vector3(x, y, 0f));
			}
		}
	}

	/// <summary>
	/// Sets up the appropriate board.
	/// </summary>
	/// <param name="level">Level to generate.</param>
	void BoardSetup (int level) {
		InitialiseList ();
		stage=level;
		//Instantiate Board and set boardHolder to its transform.
		boardHolder = new GameObject ("Board").transform;

		if (stage == 1) {
			for (int i=0; i<gridPositions.Count; i++) {
				GameObject instance = Instantiate (tiles[stage1[i]], gridPositions[i], Quaternion.identity) as GameObject;
				instance.transform.SetParent (boardHolder);
			}
		}
	}

	/// <summary>
	/// Sets up the scene. Basically public accessor for BoardSetup.
	/// </summary>
	/// <param name="level">Level to generate.</param>
	public void SetupScene (int level) {
		BoardSetup (level);
	}

	/// <summary>
	/// Find location of spawn tile.
	/// </summary>
	/// <returns>The location of desired spawn.</returns>
	/// <param name="spawnNo">Number of the desired spawn location (max. 2)</param>
	public Vector3 getSpawn(int spawnNo) {
		for (int i=0; i<stage1.Length; i++) {
			if (spawnNo==1) {
				if ((stage1[i]==62) || (stage1[i]==63)) 
					return new Vector3(gridPositions[i].x,gridPositions[i].y,gridPositions[i].z);
			}
			else if (spawnNo==2) {
					if ((stage1[i]==64) || (stage1[i]==65)) 
						return new Vector3(gridPositions[i].x,gridPositions[i].y,gridPositions[i].z);
			}
			else if (spawnNo==3) {
					if ((stage1[i]==66) || (stage1[i]==67)) 
						return new Vector3(gridPositions[i].x,gridPositions[i].y,gridPositions[i].z);
			}
			else if (spawnNo==4) {
					if (stage1[i]==68) 
						return new Vector3(gridPositions[i].x,gridPositions[i].y,gridPositions[i].z);
			}
			else if (spawnNo==5) {
					if ((stage1[i]==69) || (stage1[i]==70)) 
						return new Vector3(gridPositions[i].x,gridPositions[i].y,gridPositions[i].z);
			}
			else if (spawnNo==6) {
					if ((stage1[i]==71) || (stage1[i]==72)) 
						return new Vector3(gridPositions[i].x,gridPositions[i].y,gridPositions[i].z);
			}
			else if (spawnNo==7) {
					if (stage1[i]==73)
						return new Vector3(gridPositions[i].x,gridPositions[i].y,gridPositions[i].z);
			}
			else if ((stage1[i]==74) || (stage1[i]==75)) 
					return new Vector3(gridPositions[i].x,gridPositions[i].y,gridPositions[i].z);
		}
		return new Vector3(4f,2f,0f);
	}

	/// <summary>
	/// Gets the coordinates of the checkpoint.
	/// </summary>
	/// <returns>Vector3 coordinates of checkpoint.</returns>
	public Vector3 getCheckpoint() {
		for (int i=0; i<stage1.Length; i++) {
			if (stage1[i]==76)
				return new Vector3(gridPositions[i].x,gridPositions[i].y,gridPositions[i].z);
		}
		return new Vector3(4f,2f,0f);
	}

	/// <summary>
	/// Gets the type of the tile.
	/// </summary>
	/// <returns>The tile type.</returns>
	/// <param name="pos">Coordinates to check for type.</param>
	public string getTileType(Vector2 pos) {
		for (int i=0; i<gridPositions.Count; i++) {
			if ((gridPositions[i].x==pos.x) && (gridPositions[i].y==pos.y)) return tiles[stage1[i]].name;
		}
		return "";
	}
}
